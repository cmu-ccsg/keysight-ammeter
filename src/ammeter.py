'''
Created on Aug 12, 2019

@author: bob
'''
 
from threading import Thread
from serial import Serial
import time
import collections
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import serial.tools.list_ports
import configparser
from statistics import mean
# from matplotlib.widgets import Button
# import struct
# import pandas as pd
 
 
class serialPlot:
    def __init__(self, serialPort = '/dev/usbserial', serialBaud = 9600, prologixP = False, plotLength = 100, dataNumBytes = 2):
        self.port = serialPort
        self.baud = serialBaud
        self.prologixP = prologixP
        self.plotMaxLength = plotLength
        self.dataNumBytes = dataNumBytes
        self.rawData = bytearray(dataNumBytes)
        self.data = collections.deque([0] * plotLength, maxlen=plotLength)
        self.avgData = collections.deque([0] * plotLength, maxlen=plotLength)
        self.weights = [0] * plotLength
        self.isRun = True
        self.isReceiving = False
        self.thread = None
        self.plotTimer = 0
        self.previousTimer = 0
        # self.csvData = []
        
        print('Trying to connect to: ' + str(serialPort) + ' at ' + str(serialBaud) + ' BAUD.')
        try:
            self.serialConnection = Serial(serialPort, serialBaud, timeout=4)
            print('Connected to ' + str(serialPort) + ' at ' + str(serialBaud) + ' BAUD.')
        except:
            print("Failed to connect with " + str(serialPort) + ' at ' + str(serialBaud) + ' BAUD.  Exiting.')
            quit()
        self.sendDeviceClear()
 
    def readSerialStart(self):
        if self.thread == None:
            self.thread = Thread(target=self.backgroundThread)
            self.thread.start()
            # Block till we start receiving values
            while self.isReceiving != True:
                time.sleep(0.1)
 
    def getSerialData(self, frame, lines, lineValueText, lineLabel, timeText):
        if self.isReceiving == True:
            currentTimer = time.perf_counter()
            self.plotTimer = int((currentTimer - self.previousTimer) * 1000)     # the first reading will be erroneous
            self.previousTimer = currentTimer
            timeText.set_text('Plot Interval = ' + str(self.plotTimer) + 'ms')
            #value,  = struct.unpack('f', self.rawData)    # use 'h' for a 2 byte integer
            
            byteString = self.rawData.decode('utf-8')
            value = float(byteString) * 1e6  # convert from microamps
            
            self.data.append(value)         # we get the latest data point and append it to our array
            
            # meanValue = mean(self.data)
            # Linear weighting of samples, most-recently-appended has weight 
            length = len(self.data)
            runningTotal = 0
            runningWeight = 0
            #for i in range(0, length):
            start = max(0, length - 100)
            stop = length
            span = stop - start
            
            for i in range(start, stop):
                weight = (i+1) / span
                runningWeight += weight
                runningTotal += self.data[i] * weight  
            meanValue = runningTotal / runningWeight 
            
            self.avgData.append(meanValue)
            
            # Plotting multiple lines with animation: https://stackoverflow.com/questions/23049762/matplotlib-multiple-animate-multiple-lines
            
            lines[0].set_data(range(self.plotMaxLength), self.data)
            
            # Added this
            lines[1].set_data(range(self.plotMaxLength), self.avgData)
            
            lineValueText[0].set_text('[' + lineLabel[0] + '] = ' + "{:.1f}".format(value))   #str(value))
            lineValueText[1].set_text('[' + lineLabel[1] + '] = ' + "{:.1f}".format(meanValue))   #str(value))
            # self.csvData.append(self.data[-1])
        
    def sendCommand(self, command, readback=True):
        self.serialConnection.write((command + '\r\n').encode('utf-8'))
        time.sleep(0.1)
        if readback:
            print('looking for readback')
            response = self.serialConnection.readline()
            print('Received: <', response, '>')
            return response
        
    def sendDeviceClear(self):
        # DEVICE CLEAR is a GPIB bus-level command.  It is accessible via the serial interface
        # to the Prologix GPIB USB interface by issuing a Prologix command.
        # 
        # ALL Prologix commands begin with "++" and end with a line termination
        # Device Clear is "++clr"
        # http://prologix.biz/downloads/PrologixGpibUsbManual-4.2.pdf
        if self.prologixP:
            self.serialConnection.write(('++clr\r').encode('utf-8'))
        self.sendCommand('\x03', readback=False)  # Send ctrl-C in case the meter is in a READ? loop
        self.sendCommand('*RST', readback=False)
        self.sendCommand('*CLS', readback=False)  # Clear any errors in the instrument's queue
        return
 
    def backgroundThread(self): 
        try:   
            self.serialConnection.reset_input_buffer()
        except:
            print('Error establishing connection to the instrument.  Exiting.')
            self.close()
            quit()

        try:
            self.sendCommand('SYSTEM:REMOTE', readback=False)
            self.sendCommand('CONFIGURE:CURRENT:DC 1, 0.000005', readback=False)
            self.sendCommand('TRIGGER:SOURCE IMMEDIATE', readback=False)
            self.sendCommand('TRIGGER:DELAY 0.15', readback=False)
            self.sendCommand('TRIGGER:COUNT INFINITE', readback=False)
            self.sendCommand('SAMPLE:COUNT 1', readback=False)
            self.sendCommand('READ?', readback=False)
        except:
            print('Error configuring the instrument.  Exiting.')
            self.close()
            quit()

        try:
            while (self.isRun):
                length = self.serialConnection.readinto(self.rawData)
                if length == 0:
                    self.isReceiving = False
                    return
                self.serialConnection.read(1)   # eat the trailing comma
                self.isReceiving = True
        except:
            print('Error reading measurements from the instrument.  Exiting.')
            self.close()
            quit()
 
    def close(self):
        self.isRun = False
        self.thread.join()
        self.sendDeviceClear();
        self.sendCommand('SYSTEM:LOCAL', readback=False)
        self.serialConnection.close()
        print('Disconnected...')
        #df = pd.DataFrame(self.csvData)
        #df.to_csv('/home/bob/Desktop/data.csv')
 
 
def main():
    print('Power cycle the ammeter before starting (else serial port may hang)\r\n')
    
    comlist = serial.tools.list_ports.comports()
    connected = []
    for element in comlist:
        connected.append(element.device)
        
    print("Connected serial ports: " + str(connected))
    
    config = configparser.ConfigParser()
    config.read('ammeter.conf')

    print(config)
    
    try:
        portName = config['serial']['portName']
    except:
        print('No portName in ammeter.conf')
        portName = comlist[1].device
        
    try:
        baudRate = config['serial']['baudRate']
    except:
        print('No baudRate in ammeter.conf')
        baudRate = 9600
        
    try:
        prologix = config['serial']['prologix']
    except:
        print('No prologix flag in ammeter.conf')
        prologix = False
        
    maxPlotLength = 800
    dataNumBytes = 15        # number of bytes of 1 data point
    s = serialPlot(portName, baudRate, prologix, maxPlotLength, dataNumBytes)   # initializes all required variables
    s.readSerialStart()                                               # starts background thread
 
    # plotting starts below
    pltInterval = 50    # Period at which the plot animation updates [ms]
    xmin = 0
    xmax = maxPlotLength
    ymin = 1  # 0
    ymax = 1e7 # 500
    fig = plt.figure()
    #ax = plt.axes(xlim=(xmin, xmax),  ylim=(float(ymin - (ymax - ymin) / 10), float(ymax + (ymax - ymin) / 10)))
    ax = plt.axes(xlim=(xmin, xmax),  yscale="log", ylim=(ymin, ymax))
    ax.set_title('34401A Current Probe')
    ax.set_xlabel("time")
    ax.set_ylabel("Current (uA)")
 
    lineLabel = ['Current (uA)', 'Avg Current (uA)']
    timeText = ax.text(0.50, 0.95, '', transform=ax.transAxes)
    
    # Commented this out
    # lines = ax.plot([], [], label=lineLabel)[0]
    
    # Added this
    colors = ["black","red"]
    lines = []
    for index in range(2):
        lobj = ax.plot([],[],label=lineLabel[index], color=colors[index])[0]
        lines.append(lobj)
    
    
    
    lineValueText = [ax.text(0.50, 0.90, '', transform=ax.transAxes), ax.text(0.50, 0.85, '', transform=ax.transAxes)]
    anim = animation.FuncAnimation(fig, s.getSerialData, fargs=(lines, lineValueText, lineLabel, timeText), interval=pltInterval)    # fargs has to be a tuple
 
    plt.legend(loc="upper left")
    plt.show()
 
    s.close()
 
 
if __name__ == '__main__':
    main()